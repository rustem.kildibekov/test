﻿// Source.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "nlohmann/json.hpp"

// for convenience
using json = nlohmann::json;

using namespace std;
using json = json;

struct Setting {
    int dataNum;
    int vectorSize;
    string inputFileName;
    string outputFileName;
};


struct coefficients {
    coefficients() {
        a = 0;
        b = 0;
    }
    double a, b;
};

struct DataSet {
    double x, y, cY;
    DataSet(double _x, double _y, double _cY): x(_x), y(_y), cY(_cY) {
    }
    DataSet() {
        x = 0;
        y = 0;
        cY = 0;
    }
};

void createTestData(int xFrom, int xTo, double xStep, string fileName);
coefficients leastSquares(string fileName);
void leastSquaresCalulate(string inFileName, string outFileName, coefficients coef, int vectorSize);
void writeToFile(vector<DataSet>& dataList, string outFileName);
Setting readSettingFile(string settingFileName);


int main()
{
    Setting setting = readSettingFile("settings.json");


    cout << "Press 1 - Create test data" << endl;
    cout << "Press 2 - Start calaculation" << endl;
    cout << "Press 0 - Exit" << endl;

    int inputCode;
    do {
        cin >> inputCode;
        if (inputCode == 1) {
            cout << "Creating test data" << endl;
            createTestData(0, setting.dataNum, 0.1, setting.inputFileName);
        }
        else if (inputCode == 2) {
            cout << "Processing" << endl;
            coefficients coef = leastSquares("in.dat");
            leastSquaresCalulate(setting.inputFileName, setting.outputFileName, coef, setting.vectorSize);
        }
           
    } while (inputCode != 0);
    return 0;
}

void createTestData(int xFrom, int xTo, double xStep, string fileName) {
    ofstream file;
    file.open(fileName, ios::out | ios::trunc);

    int n = 0;
    for (double x = xFrom; x <= xTo; x += xStep) {
        
        double&& y = sin(x) + rand() % (int)(x+1);
        file << x << " " << y << endl;
        ++n;
        if (n % 1000000 == 0) cout << "n = " << n <<  endl;
    }
    file.close();
    cout << "Test data saved in " + fileName << endl;
}

coefficients leastSquares(string fileName) {
    ifstream file;
    int n = 0;
    double x, y;
    double sumX = 0;
    double sumY = 0;
    double sumX2 = 0;
    double sumXY = 0;
    char buffer[100];
    file.rdbuf()->pubsetbuf(buffer, 100);
    file.open(fileName, ios::in);
    if (!file.is_open()) {
        cout << "File isn't open" << endl;
        return coefficients();
    }

    while (file >> x >> y) {
        ++n;
        sumX += x;
        sumY += y;
        sumX2 += x * x;
        sumXY += x * y;
    }

    //cout << "n =" << n << "; sumX = " << sumX << "; sumY = " << sumY << "; sumX2 = " << sumX2 << "; sumXY =" << sumXY << endl;

    /*
        sumX2 sumX      sumXY
        sumX  n         sumY

    */

    //Обратная матрица
    double deltaA = sumX2 * n - sumX * sumX;
    cout << "deltaA =" << deltaA << endl;
    double m11, m12, m21, m22;
    m11 =  n / deltaA;
    m12 = (sumX * (-1))/ deltaA;
    m21 = (sumX * (-1))/ deltaA;
    m22 = sumX2 / deltaA;

    //cout << "m11 =" << m11 << "; m12 =" << m12 << "m21 =" << m21 << "; m22 =" << m22 << endl;

    //Умножение матрицы
    coefficients coef;
    coef.a = m11 * sumXY + m12 * sumY;
    coef.b = m21 * sumXY + m22 * sumY;
    cout << "coef.a = " << coef.a << "; coef.b =" << coef.b << endl;
    return coef;
}

void leastSquaresCalulate(string inFileName, string outFileName, coefficients coef, int vectorSize) {
    ifstream file;
    
    file.open(inFileName, ios::in);
    if (!file.is_open()) {
        cout << "File isn't open" << endl;
        return;
    }

    vector<DataSet> dataList;
    dataList.resize(vectorSize);
    int datalistCount = 0;
    double x, y;

    //Очистка файла
    fstream outFile(outFileName, std::ios::out);
    outFile.close();


    while (file >> x >> y) {
        dataList[datalistCount] = DataSet(x, y, coef.a * x + coef.b);
        ++datalistCount;
        if (datalistCount >= (int)dataList.size()) {
            writeToFile(dataList, outFileName);
            datalistCount = 0;
        }
    }

    if (datalistCount > 0) {
        dataList.resize(datalistCount);
        writeToFile(dataList, outFileName);
    }

    file.close();
    cout << "Result saved in " + outFileName << endl;
}

void writeToFile(vector<DataSet>& dataList, string outFileName) {
    ofstream file;
    file.open(outFileName, ios::app);
    for (DataSet& data : dataList) {
        file << data.x << " " << data.y << " " << data.cY << endl;
    }
    file.close();
}

Setting readSettingFile(string settingFileName) {
    Setting result;

    std::ifstream ifs(settingFileName);
    json jf = json::parse(ifs);
    
    result.dataNum = jf["dataNum"].get<int>();
    result.vectorSize = jf["vectorSize"].get<int>();
    result.inputFileName = jf["inputFileName"].get<string>();
    result.outputFileName = jf["outputFileName"].get<string>();
    ifs.close();
    

    return result;
}